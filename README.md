# µWebserver

µWebserver is a micro web server written in Java and based on Apache HttpCore. It is useful for running locally web applications that need to make ajax requests on static resources. It features:

- automatic launching of the web browser when launching the server (configurable),
- REST API to shutdown the server
- REST heartbeat API to automatically shutdown the server (configurable)
- tray icon with activity status and menu (stop the server, launch the browser, configure)
- configuration UI