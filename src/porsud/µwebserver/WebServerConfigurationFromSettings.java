package porsud.µwebserver;

import java.io.IOException;
import java.net.Socket;

/**
 * A {@link WebServer.Configuration} based on an instance of {@link Settings}
 * @author Fabien Mairesse
 */
public class WebServerConfigurationFromSettings implements WebServer.Configuration {

	private int port = -1;
	private Settings settings;

	public WebServerConfigurationFromSettings(Settings settings) {
		this.settings = settings;
	}

	@Override
	public int port() {
		if (this.port == -1) {
			Integer port = this.settings.port();
			if (port == null) {
				this.port = getNextAvailableServerPort(8080);
			} else {
				this.port = port;
			}
		}
		return this.port;
	}

	@Override
	public String docRoot() {
		return this.settings.docRoot();
	}

	@Override
	public boolean showExitButton() {
		return this.settings.showExitButton();
	}

	@Override
	public int heartbeatTimeout() {
		return this.settings.heartbeatTimeout();
	}

	@Override
	public int serverCheckTimeout() {
		return this.settings.serverCheckTimeout();
	}

	private static int getNextAvailableServerPort(int port) {
		Socket s = null;
		while (true) {
			try {
				s = new Socket("localhost", port);
				// If the code makes it this far without an exception it means
				// something is using the port and has responded.
				port += 1;
			} catch (IOException e) {
				return port;
			} finally {
				if (s != null) {
					try {
						s.close();
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
	}
}
