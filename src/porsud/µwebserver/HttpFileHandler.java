package porsud.µwebserver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.nio.entity.NFileEntity;
import org.apache.http.nio.entity.NStringEntity;

import porsud.µwebserver.resources.Resources;

/**
 * @author Fabien Mairesse
 */
public class HttpFileHandler extends BasicHttpAsyncRequestHandler {

	private static Map<String, String> FILEEXTENSION_TO_MIMETYPE = new HashMap<String, String>();
	static {
		FILEEXTENSION_TO_MIMETYPE.put(null, "application/octet-stream");

		FILEEXTENSION_TO_MIMETYPE.put("html", "text/html");
		FILEEXTENSION_TO_MIMETYPE.put("xml", "text/xml");
		FILEEXTENSION_TO_MIMETYPE.put("js", "text/javascript");
		FILEEXTENSION_TO_MIMETYPE.put("css", "text/css");
		FILEEXTENSION_TO_MIMETYPE.put("appcache", "text/cache-manifest");

		FILEEXTENSION_TO_MIMETYPE.put("jpg", "image/jpeg");
		FILEEXTENSION_TO_MIMETYPE.put("jpeg", "image/jpeg");
		FILEEXTENSION_TO_MIMETYPE.put("png", "image/png");
		FILEEXTENSION_TO_MIMETYPE.put("svg", "image/svg+xml");
		FILEEXTENSION_TO_MIMETYPE.put("tiff", "image/tiff");
		FILEEXTENSION_TO_MIMETYPE.put("gif", "image/gif");

		FILEEXTENSION_TO_MIMETYPE.put("pdf", "application/pdf");

		FILEEXTENSION_TO_MIMETYPE.put("ogv", "video/ogg");
		FILEEXTENSION_TO_MIMETYPE.put("ogg", "application/ogg");
		FILEEXTENSION_TO_MIMETYPE.put("mp4", "video/mp4");

	}

	private final File docRoot;

	public HttpFileHandler(final File docRoot) {
		super();
		this.docRoot = docRoot;
	}

	protected void handle(final HttpRequest request, final HttpResponse response) throws HttpException, IOException {

		String method = request.getRequestLine().getMethod().toUpperCase();
		boolean sendEntity = method.equals("GET");

		if (!(sendEntity || method.equals("HEAD"))) {
			throw new MethodNotSupportedException(method + " method not supported");
		}

		URL url = new URL("http://foo.com"+request.getRequestLine().getUri());
		String target = url.getPath();
		if (target.startsWith("/")) {
			target = target.substring(1);
		}
		if (target.isEmpty()) {
			target = "index.html";
		}
		final File file = new File(this.docRoot, URLDecoder.decode(target, "UTF-8"));
		if (!file.exists()) {

			// Special case if requesting unexisting favicon
			if (target.equals("favicon.ico")) {
				InputStream input = Resources.getResource("favicon.ico");
				try {
					InputStreamEntity entity = new InputStreamEntity(input);
					response.setEntity(entity);
				} finally {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				response.setStatusCode(HttpStatus.SC_NOT_FOUND);
				response.setReasonPhrase("Not Found");
				if (sendEntity) {
					NStringEntity entity = new NStringEntity("<html><body><h1>File " + file.getPath() + " not found</h1></body></html>", ContentType.create("text/html", "UTF-8"));
					response.setEntity(entity);
				}
				System.out.println("File " + file.getPath() + " not found");
			}

		} else if (!file.canRead() || file.isDirectory()) {

			response.setStatusCode(HttpStatus.SC_FORBIDDEN);
			response.setReasonPhrase("Forbidden");
			if (sendEntity) {
				NStringEntity entity = new NStringEntity("<html><body><h1>Access denied</h1></body></html>", ContentType.create("text/html", "UTF-8"));
				response.setEntity(entity);
			}
			System.out.println("Cannot read file " + file.getPath());

		} else {
			if (sendEntity) {
				// Guess content-type from file extension
				String[] splittedFilePath = file.getName().split("\\.");
				String fileExtension;
				if (splittedFilePath.length >= 2) {
					fileExtension = splittedFilePath[splittedFilePath.length - 1];
				} else {
					fileExtension = null;
				}
				if (!FILEEXTENSION_TO_MIMETYPE.containsKey(fileExtension)) {
					fileExtension = null;
				}
				String contentType = FILEEXTENSION_TO_MIMETYPE.get(fileExtension);
				response.setStatusCode(HttpStatus.SC_OK);
				NFileEntity body = new NFileEntity(file, ContentType.create(contentType));
				response.setEntity(body);
			}
			System.out.println("Serving file " + file.getPath());
		}
	}

}