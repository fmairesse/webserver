package porsud.µwebserver;

import java.io.IOException;


import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;

import porsud.json.JsonValue;

/**
 * @author Fabien Mairesse
 */
abstract class BasicJsonHttpAsyncRequestHandler extends BasicHttpAsyncRequestHandler {

	@Override
	protected void handle(HttpRequest request, HttpResponse response) throws HttpException, IOException {
		JsonValue jresponse = this.handle(request);
		if (jresponse != null) {
			NStringEntity responseEntity = new NStringEntity(jresponse.toString());
			responseEntity.setContentType(ContentType.APPLICATION_JSON.getMimeType());
			response.setEntity(responseEntity);
		}
	}

	protected abstract JsonValue handle(HttpRequest request) throws HttpException, IOException;
}
