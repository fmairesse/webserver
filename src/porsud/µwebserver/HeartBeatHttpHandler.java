package porsud.µwebserver;

import java.io.IOException;
import java.util.Date;
import java.util.Map;


import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.util.EntityUtils;

import porsud.json.JsonNumber;
import porsud.json.JsonObject;
import porsud.json.JsonString;
import porsud.json.JsonValue;


/**
 * Handles HTTP requests notifiying signs of life from clients.
 * @author Fabien Mairesse
 */
public class HeartBeatHttpHandler extends BasicJsonHttpAsyncRequestHandler {

	private Map<String, Date> heartbeats;
	private int timeout;

	/**
	 * @param heartbeats Maps a client identifier to its last heartbeat time
	 * @param timeout The time between two heartbeats beyond which a client is considered as dead 
	 */
	public HeartBeatHttpHandler(Map<String, Date> heartbeats, int timeout) {
		this.heartbeats = heartbeats;
		this.timeout = timeout;
	}

	@Override
	public JsonValue handle(HttpRequest request) throws HttpException, IOException {
		// Request encloses an HttpEntity if the client sent a body in the message
		// This body should be the identifier of the client.
		// If the message does not contain an identifier, it should be the first heartbeat
		// request and the identifier is supplied by the server

		JsonValue jresponse;
	
		// Try to get the HttpEntity
		HttpEntity entity;
		try {
			entity = ((HttpEntityEnclosingRequest) request).getEntity();
		} catch (ClassCastException e) {
			entity = null;
		}

		// Try to read the identifier of the client
		String id;
		if (entity == null) {
			id = null;
		} else {
			id = EntityUtils.toString(entity);
			if (id.isEmpty()) {
				id = null;
			}
		}

		// Send the identifier of the client if none was in the message
		if (id == null) {
			synchronized (this.heartbeats) {
				id = String.valueOf(this.heartbeats.size() + 1);
				this.heartbeats.put(id, new Date());
			}
			JsonObject jresponseAsObject = new JsonObject();
			jresponseAsObject.put(Protocol.Heartbeat.Response.ID, new JsonString(id));
			jresponseAsObject.put(Protocol.Heartbeat.Response.TIMEOUT, new JsonNumber(this.timeout));
			jresponse = jresponseAsObject;
		} else { // Update the map of clients heartbeat
			synchronized (this.heartbeats) {
				this.heartbeats.put(id, new Date());
			}
			jresponse = null;
		}
		
		return jresponse;
	}
}