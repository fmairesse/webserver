package porsud.µwebserver;

/**
 * @author Fabien Mairesse
 */
public interface Protocol {

	/**
	 * Tag interface.
	 */
	interface Path {
	}

	class Utils {

		private Utils() {
		}

		public static String getPath(Class<? extends Path> pathAsClass) {
			String path = pathAsClass.getName();
			path = path.substring(path.indexOf('$') + 1);
			path = path.replace('$', '/').toLowerCase();
			return '/'+path;
		}
	}

	interface Heartbeat extends Path {
		interface Response {
			String ID = "id";
			String TIMEOUT = "timeout";
		}
	}

	interface Stop extends Path {
	}

	interface Configuration extends Path {
		String SHOW_EXIT_BUTTON = "showexitbutton";
	}
}
