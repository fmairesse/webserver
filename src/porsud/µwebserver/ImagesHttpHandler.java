package porsud.µwebserver;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NByteArrayEntity;
import org.apache.http.nio.entity.NFileEntity;

public class ImagesHttpHandler extends BasicHttpAsyncRequestHandler {

	private static final Map<String, String> FILEEXTENSION_TO_MIMETYPE = new HashMap<String, String>();
	static {
		FILEEXTENSION_TO_MIMETYPE.put("jpg", "image/jpeg");
		FILEEXTENSION_TO_MIMETYPE.put("jpeg", "image/jpeg");
		FILEEXTENSION_TO_MIMETYPE.put("png", "image/png");
		
		ImageIO.setUseCache(false);
	}

	private static final String[] WIDTH_PARAMETER_NAMES = new String[] { "w", "width" };
	private static final String[] HEIGHT_PARAMETER_NAMES = new String[] { "h", "height" };

	private static final Map<String, HttpEntity> responsesCache = Collections.synchronizedMap(new ImagesCache());

	private final File docRoot;

	public ImagesHttpHandler(final File docRoot) {
		super();
		this.docRoot = docRoot;
	}

	@Override
	protected void handle(HttpRequest request, HttpResponse response) throws HttpException, IOException {

		String method = request.getRequestLine().getMethod().toUpperCase();
		boolean sendEntity = method.equals("GET");

		if (!(sendEntity || method.equals("HEAD"))) {
			throw new MethodNotSupportedException(method + " method not supported");
		}

		URL url = new URL("http://foo.com"+request.getRequestLine().getUri());
		String target = url.getPath();

		final File file = new File(this.docRoot, URLDecoder.decode(target, "UTF-8"));
		if (!file.exists()) {
			response.setStatusCode(HttpStatus.SC_NOT_FOUND);
			response.setReasonPhrase("Not Found");
			System.out.println("File " + file.getPath() + " not found");

		} else if (!file.canRead() || file.isDirectory()) {

			response.setStatusCode(HttpStatus.SC_FORBIDDEN);
			response.setReasonPhrase("Forbidden");
			System.out.println("Cannot read file " + file.getPath());

		} else {

			if (sendEntity) {
				// Guess content-type from file extension
				String[] splittedFilePath = file.getName().split("\\.");
				String fileExtension;
				if (splittedFilePath.length >= 2) {
					fileExtension = splittedFilePath[splittedFilePath.length - 1];
				} else {
					fileExtension = null;
				}
				String contentType = FILEEXTENSION_TO_MIMETYPE.get(fileExtension);
				
				if (contentType == null) {

					response.setStatusCode(HttpStatus.SC_BAD_REQUEST);
					response.setReasonPhrase("Bad Request");
					System.out.println("Bad request " + request.getRequestLine().getUri());

				} else {
				
					// Extract dimension parameters
					String widthStr = null, heightStr = null;
					String query = url.getQuery();
					if (query != null) {
						String[] parameters = query.split("&");
						for (String p : parameters) {
							String[] parameter = p.split("=");
							if (parameter.length == 2) {
								String name = parameter[0].toLowerCase();
								if (Arrays.binarySearch(WIDTH_PARAMETER_NAMES, name) != -1) {
									widthStr = parameter[1];
								} else if (Arrays.binarySearch(HEIGHT_PARAMETER_NAMES, name) != -1) {
									heightStr = parameter[1];
								}
							}
						}
					}
	
					int status = HttpStatus.SC_OK;
					HttpEntity entity = null;
	
					if (widthStr != null || heightStr != null) {
						int width = -1, height = -1;
						try {
							if (widthStr != null) {
								width = Integer.parseInt(widthStr);
							}
							if (heightStr != null) {
								height = Integer.parseInt(heightStr);
							}
						} catch (NumberFormatException e) {
							status = HttpStatus.SC_BAD_REQUEST;
							response.setReasonPhrase("Bad Request");
							System.out.println("Bad request " + request.getRequestLine().getUri());
						}
						if (status == HttpStatus.SC_OK) {
							String cachePath = file.getAbsolutePath() + ':' + width + 'x' + height;
							entity = responsesCache.get(cachePath);
							if (entity == null) {
								BufferedImage srcImage = ImageIO.read(file);
								if (width == -1) {
									width = srcImage.getWidth() * height / srcImage.getHeight();
								} else if (height == -1) {
									height = srcImage.getHeight() * width / srcImage.getWidth();
								}
								// Create image writer
								ImageWriter writer = ImageIO.getImageWritersByFormatName(fileExtension).next();
								ImageWriteParam param = writer.getDefaultWriteParam();
								param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT); // Needed see javadoc
								param.setCompressionQuality(1.0F); // Highest quality

								BufferedImage resizedImage = resizeImage(srcImage, width, height);
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								ImageIO.write(resizedImage, fileExtension, baos);
								try {
									baos.flush();
									entity = new NByteArrayEntity(baos.toByteArray(), ContentType.create(contentType));
								} finally {
									baos.close();
								}
								
//								// Resize image
//								IIOImage resizedImage = new IIOImage(resizeImage(srcImage, width, height), null, null);
//								
//								// Write image
//								ByteArrayOutputStream baos = new ByteArrayOutputStream();
//								try {
//									ImageOutputStream ios = new MemoryCacheImageOutputStream(baos);
//									try {
//										writer.setOutput(ios);
//										writer.write(resizedImage);
//									} finally {
//										ios.close();
//									}
//									entity = new NByteArrayEntity(baos.toByteArray(), ContentType.create(contentType));
//								} finally {
//									baos.close();
//								}
								responsesCache.put(cachePath, entity);
							} else {
								// re-put entity to update access time of cache
								responsesCache.put(cachePath, responsesCache.remove(cachePath));
							}
							System.out.println("Serving file " + file.getPath() + " " + query);
						}
					} else  { // no dimension specified in url parameters
						entity = new NFileEntity(file, ContentType.create(contentType));
						System.out.println("Serving file " + file.getPath());
					}

					// Send response
					response.setStatusCode(status);
					response.setEntity(entity);
				}
				
			}
		}
	}

	private static BufferedImage resizeImage(BufferedImage srcImage, int width, int height) {
		int type = srcImage.getType();
		if (type == 0) {
			type = BufferedImage.TYPE_INT_ARGB;
		}
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(srcImage, 0, 0, width, height, null);
		g.dispose();

		return resizedImage;
	}

	@SuppressWarnings("serial")
	private static class ImagesCache extends LinkedHashMap<String, HttpEntity> {
		
		private static final int CAPACITY = 50 * 1024; // 50MB

		private long size = 0;

		@Override
		protected boolean removeEldestEntry(java.util.Map.Entry<String, HttpEntity> eldest) {
			return size >= CAPACITY;
		}
		
		@Override
		public HttpEntity put(String key, HttpEntity value) {
			HttpEntity prev = super.put(key, value);
			if (value != null) {
				size += value.getContentLength();
			}
			return prev;
		}

		@Override
		public HttpEntity remove(Object key) {
			HttpEntity prev = super.remove(key);
			if (prev != null) {
				size -= prev.getContentLength();
			}
			return prev;
		}
	}
}
