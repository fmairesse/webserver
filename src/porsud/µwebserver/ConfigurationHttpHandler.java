package porsud.µwebserver;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;

import porsud.json.JsonBoolean;
import porsud.json.JsonObject;
import porsud.json.JsonValue;


/**
 * An HTTP handler that provides values of the current configuration.
 * @author Fabien Mairesse
 */
public class ConfigurationHttpHandler extends BasicJsonHttpAsyncRequestHandler {

	private boolean showExitButton;

	public ConfigurationHttpHandler(boolean showExitButton) {
		this.showExitButton = showExitButton;
	}

	@Override
	protected JsonValue handle(HttpRequest request) throws HttpException, IOException {
		JsonObject jconfiguration = new JsonObject();
		jconfiguration.put(Protocol.Configuration.SHOW_EXIT_BUTTON, new JsonBoolean(this.showExitButton));
		return jconfiguration;
	}

}
