package porsud.µwebserver;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.UIManager;

import porsud.µwebserver.resources.Resources;


/**
 * Provides the tray icon UI.
 * @author Fabien Mairesse
 */
class Tray {

	private WebServer server;
	private Settings settings;
	private StopFlag stopFlag;
	private SystemTray tray;
	private TrayIcon trayIcon;
	private ServerStatusWatcher serverStatusWatcher;
	private SettingsDialog settingsDialog;
	private SettingsSavedCallback settingsSavedCallback;

	static {
		// Set system look and feel
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
	}

	public Tray(WebServer server, Settings settings, StopFlag stopFlag) {
		if (!SystemTray.isSupported())
			return;

		this.server = server;
		this.settings = settings;
		this.stopFlag = stopFlag;
		this.tray = SystemTray.getSystemTray();
		this.trayIcon = new TrayIcon(Resources.icon(), "µWebServer");

		// Watch running status of the server
		this.serverStatusWatcher = new ServerStatusWatcher();
		new Thread(this.serverStatusWatcher, "TrayServerStatusWatcher").start();

		// Watch connection status of the server
		this.server.addConnectionListener(new ConnectionListener());

		// Watch change of settings to update tray menu
		this.settingsSavedCallback = new SettingsSavedCallback();
		this.settings.addSavedCallback(this.settingsSavedCallback);

		updatePopup();

		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			System.err.println("TrayIcon could not be added.");
		}
	}

	public void close() {
		if (this.tray != null) {
			this.settings.removeSavedCallback(this.settingsSavedCallback);
			if (this.settingsDialog != null) {
				this.settingsDialog.dispose();
			}
			this.tray.remove(trayIcon);
			serverStatusWatcher.stop = true;
		}
	}

	public void setIcon(Image image) {
		if (this.tray != null) {
			this.trayIcon.setImage(image);
		}
	}

	public void showError(String message, Exception e) {
		if (this.tray != null) {
			this.trayIcon.displayMessage("Error", message + "\n" + e.getMessage(), TrayIcon.MessageType.ERROR);
		}
	}

	private void updatePopup() {
		PopupMenu popup = new PopupMenu();
		this.trayIcon.setPopupMenu(popup);

		final Browser browser = new Browser(this.settings.browser());
		if (browser.canOpen()) {
			// Launch browser menu item
			MenuItem launchBrowserItem = new MenuItem("Launch web browser");
			launchBrowserItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					try {
						browser.open(Tray.this.server.getRootURL(settings.startUrl()));
					} catch (IOException e) {
						Tray.this.showError("Couldn't launch web browser", e);
					}
				}
			});
			popup.add(launchBrowserItem);
		}

		// Copy URL to clipboard
		MenuItem copyURLItem = new MenuItem("Copy URL to clipboard");
		copyURLItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				StringSelection url = new StringSelection(Tray.this.server.getRootURL(settings.startUrl()));
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(url, null);
			}
		});
		popup.add(copyURLItem);

		popup.addSeparator();

		// Settings
		MenuItem settingsItem = new MenuItem("Settings");
		settingsItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Tray.this.settingsDialog == null) {
					Tray.this.settingsDialog = new SettingsDialog(Tray.this.settings);
				}
				Tray.this.settingsDialog.setVisible(true);
			}
		});
		popup.add(settingsItem);
		
		popup.addSeparator();

		// Stop menu item
		MenuItem stopItem = new MenuItem("Shutdown");
		stopItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				Tray.this.stopFlag.set();
			}
		});
		popup.add(stopItem);
	}

	private class ConnectionListener implements WebServer.ConnectionListener {
		@Override
		public void connectionOpen() {
			setIcon(Resources.connectedIcon());
		}
		@Override
		public void connectionClosed() {
			setIcon(Resources.icon());
		}
	}

	private class ServerStatusWatcher implements Runnable {
		boolean stop = false;

		@Override
		public void run() {
			while (this.stop == false) {
				synchronized (Tray.this.server) {
					WebServer.RunningStatus runningStatus = Tray.this.server.getRunningStatus();
					Image icon = runningStatus == WebServer.RunningStatus.SERVING ? Resources.icon() : Resources.stopped();
					Tray.this.setIcon(icon);
					try {
						Tray.this.server.wait(100);
					} catch (InterruptedException e) {
					}
				}
			}
		}
	}

	private class SettingsSavedCallback implements Runnable {
			@Override
		public void run() {
			Tray.this.updatePopup();
		}
	}
}
