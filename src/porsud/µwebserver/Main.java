package porsud.µwebserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Fabien Mairesse
 */
public class Main {

	public static void main(String[] args) throws Exception {

		// Load settings
		Settings settings = new Settings();

		// Get browser from settings
		Browser browser = new Browser(settings.browser());

		// Check if there we are already running in another process
		File lockFile = new File(System.getProperty("java.io.tmpdir"), ".ntrainerlock");
		boolean notLocked;
		if (lockFile.exists()) {
			notLocked = lockFile.delete();
		} else {
			notLocked = true;
		}

		// Open lock file during execution
		if (notLocked) {
			WebServer.Configuration webserverConfiguration = new WebServerConfigurationFromSettings(settings);
			FileWriter writer = new FileWriter(lockFile);
			StopFlag stopFlag = new StopFlag();
			try {
				// Write the port in the lock file
				writer.write(String.valueOf(webserverConfiguration.port()));
				writer.flush();

				// Create server
				WebServer server = new WebServer(webserverConfiguration, stopFlag);

				// Show tray
				Tray tray;
				if (settings.showTray()) {
					tray = new Tray(server, settings, stopFlag);
				} else {
					tray = null;
				}

				// Observe settings changes
				settings.addSavedCallback(new SettingsSavedCallback(server));

				// Start server
				boolean serverStarted = false;
				try {
					server.start();
					serverStarted = true;
				} catch (IOException e) {
					if (tray != null) {
						tray.showError("Cannot start server", e);
					}
				}

				if (serverStarted) {
					// Wait for server to be started
					server.waitForRunningStatus(WebServer.RunningStatus.SERVING);
	
					// Launch browser
					System.out.println("starturl="+server.getRootURL(settings.startUrl()));
					browser.open(server.getRootURL(settings.startUrl()));
	
					// Wait request to stop
					stopFlag.waitForStop();
	
					// Stop server
					server.stop();
					server.waitForRunningStatus(WebServer.RunningStatus.STOPPED);
				} else {
					// Do not die right away if tray exists: let the user see the error message and adjust settings
					if (tray != null) {
						// Wait request to stop
						stopFlag.waitForStop();
					}
				}

				// Close Tray
				if (tray != null) {
					tray.close();
				}
			} finally {
				writer.close();
				lockFile.delete();
			}
		} else {
			int port;
			BufferedReader reader = new BufferedReader(new FileReader(lockFile));
			try {
				port = Integer.parseInt(reader.readLine());
			} finally {
				reader.close();
			}
			browser.open(WebServer.getRootURL(port, settings.startUrl()));
		}

	}

	/**
	 * Observes whether the settings are saved and restart the webserver when it happens.
	 */
	private static class SettingsSavedCallback implements Runnable {
		private WebServer server;

		public SettingsSavedCallback(WebServer server) {
			this.server = server;
		}

		@Override
		public void run() {
			new Thread("SettingsSavedCallback") {
				public void run() {
					// Restart the server
					try {
						SettingsSavedCallback.this.server.restart();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}.start();
		}
	}
}
