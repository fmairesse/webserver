package porsud.µwebserver;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import porsud.json.JsonArray;
import porsud.json.JsonString;
import porsud.µwebserver.resources.Resources;


/**
 * @author Fabien Mairesse
 */
@SuppressWarnings("serial")
public class SettingsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField_webDirectory;
	private JTextField textField_webBrowserOther = new JTextField();;
	private JButton button_webBrowserOther = new JButton("…");
	private JRadioButton rdbtnBrowserIE = new JRadioButton("Internet Explorer (kiosk mode)");
	private JRadioButton rdbtnBrowserSystemDefault = new JRadioButton("System default");
	private JRadioButton rdbtnBrowserOther = new JRadioButton("Other");
	private final JRadioButton rdbtnBrowserNone = new JRadioButton("None");
	private JLabel lblWebBrowser;
	private JLabel lblWebDirectory;

	/**
	 * Create the dialog.
	 */
	public SettingsDialog(final Settings settings) {
		setIconImage(Resources.icon());
		setTitle("µWebServer Settings");
		setBounds(100, 100, 425, 223);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout sl_contentPanel = new SpringLayout();
		sl_contentPanel.putConstraint(SpringLayout.NORTH, button_webBrowserOther, 0, SpringLayout.NORTH, rdbtnBrowserOther);
		sl_contentPanel.putConstraint(SpringLayout.NORTH, textField_webBrowserOther, 1, SpringLayout.NORTH, rdbtnBrowserOther);
		sl_contentPanel.putConstraint(SpringLayout.EAST, button_webBrowserOther, 0, SpringLayout.EAST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.NORTH, rdbtnBrowserOther, 0, SpringLayout.SOUTH, rdbtnBrowserIE);
		sl_contentPanel.putConstraint(SpringLayout.WEST, rdbtnBrowserOther, 0, SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.NORTH, rdbtnBrowserIE, 0, SpringLayout.SOUTH, rdbtnBrowserSystemDefault);
		sl_contentPanel.putConstraint(SpringLayout.WEST, rdbtnBrowserIE, 0, SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.NORTH, rdbtnBrowserSystemDefault, 0, SpringLayout.SOUTH, rdbtnBrowserNone);
		sl_contentPanel.putConstraint(SpringLayout.WEST, rdbtnBrowserSystemDefault, 0, SpringLayout.WEST, contentPanel);
		sl_contentPanel.putConstraint(SpringLayout.WEST, rdbtnBrowserNone, 0, SpringLayout.WEST, contentPanel);
		contentPanel.setLayout(sl_contentPanel);
		{
			lblWebDirectory = new JLabel("Web directory:");
			sl_contentPanel.putConstraint(SpringLayout.NORTH, lblWebDirectory, 5, SpringLayout.NORTH, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.WEST, lblWebDirectory, 0, SpringLayout.WEST, contentPanel);
			contentPanel.add(lblWebDirectory);
			lblWebDirectory.setToolTipText("The directory where your web content is stored");
		}
		{
			textField_webDirectory = new JTextField(settings.docRoot());
			sl_contentPanel.putConstraint(SpringLayout.WEST, textField_webBrowserOther, 0, SpringLayout.WEST, textField_webDirectory);
			sl_contentPanel.putConstraint(SpringLayout.EAST, textField_webBrowserOther, 0, SpringLayout.EAST, textField_webDirectory);
			sl_contentPanel.putConstraint(SpringLayout.NORTH, textField_webDirectory, -3, SpringLayout.NORTH, lblWebDirectory);
			sl_contentPanel.putConstraint(SpringLayout.WEST, textField_webDirectory, 5, SpringLayout.EAST, lblWebDirectory);
			contentPanel.add(textField_webDirectory);
			textField_webDirectory.setHorizontalAlignment(SwingConstants.LEFT);
			textField_webDirectory.setToolTipText("The directory where your web content is stored");
		}
		{
			JButton button_webDirectory = new JButton("…");
			sl_contentPanel.putConstraint(SpringLayout.EAST, textField_webDirectory, -5, SpringLayout.WEST, button_webDirectory);
			sl_contentPanel.putConstraint(SpringLayout.NORTH, button_webDirectory, -4, SpringLayout.NORTH, lblWebDirectory);
			sl_contentPanel.putConstraint(SpringLayout.EAST, button_webDirectory, 0, SpringLayout.EAST, contentPanel);
			contentPanel.add(button_webDirectory);
			button_webDirectory.setToolTipText("Choose a web directory");
			button_webDirectory.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					File docRoot = new File(new File(textField_webDirectory.getText()).getAbsolutePath());
					if (!docRoot.isDirectory()) {
						docRoot = new File(new File("").getAbsolutePath());
					}
					JFileChooser fileChooser = new JFileChooser(docRoot);
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int choice = fileChooser.showDialog(SettingsDialog.this, "Choose a web directory");
					if (choice == JFileChooser.APPROVE_OPTION) {
						File selectedDocRoot = fileChooser.getSelectedFile();
						textField_webDirectory.setText(selectedDocRoot.getAbsolutePath());
					}
				}
			});
		}
		ButtonGroup radioGroup = new ButtonGroup();
		{
			lblWebBrowser = new JLabel("Web browser:");
			sl_contentPanel.putConstraint(SpringLayout.NORTH, lblWebBrowser, 10, SpringLayout.SOUTH,
					textField_webDirectory);
			sl_contentPanel.putConstraint(SpringLayout.NORTH, rdbtnBrowserNone, 5, SpringLayout.SOUTH, lblWebBrowser);
			sl_contentPanel.putConstraint(SpringLayout.WEST, lblWebBrowser, 0, SpringLayout.WEST, contentPanel);
			contentPanel.add(lblWebBrowser);
			lblWebBrowser.setHorizontalAlignment(SwingConstants.LEFT);
			lblWebBrowser.setToolTipText("The web browser to launch when the application starts");
		}
		radioGroup.add(rdbtnBrowserNone);
		contentPanel.add(rdbtnBrowserNone);
		contentPanel.add(rdbtnBrowserIE);

		rdbtnBrowserIE.setToolTipText("Launches Internet Explorer in kiosk mode (fullscreen)");
		radioGroup.add(rdbtnBrowserIE);
		contentPanel.add(rdbtnBrowserSystemDefault);
		rdbtnBrowserSystemDefault.setToolTipText("Launches the default web browser (depends on the settings of your system)");
		radioGroup.add(rdbtnBrowserSystemDefault);
		contentPanel.add(rdbtnBrowserOther);
		rdbtnBrowserOther.setToolTipText("Launches an alternate web browser");
		radioGroup.add(rdbtnBrowserOther);
		rdbtnBrowserOther.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				boolean enabled = rdbtnBrowserOther.isSelected();
				textField_webBrowserOther.setEnabled(enabled);
				button_webBrowserOther.setEnabled(enabled);
			}
		});

		{
			contentPanel.add(textField_webBrowserOther);
			textField_webBrowserOther.setEnabled(false);
			textField_webBrowserOther.setColumns(10);
		}
		{
			contentPanel.add(button_webBrowserOther);
			button_webBrowserOther.setEnabled(false);
			button_webBrowserOther.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setFileFilter(new FileFilter() {
						@Override
						public String getDescription() {
							return "Executable file";
						}

						@Override
						public boolean accept(File f) {
							if (f.isDirectory())
								return true;
							if (System.getProperty("os.name").contains("Windows")) {
								return f.getName().endsWith(".exe");
							}
							return f.canExecute();
						}
					});
					int choice = fileChooser.showDialog(SettingsDialog.this, "Choose a web browser");
					if (choice == JFileChooser.APPROVE_OPTION) {
						textField_webBrowserOther.setText(fileChooser.getSelectedFile().getAbsolutePath());
					}
				}
			});
		}
		{
			String browserCommand = settings.browser();
			if (browserCommand == null) { // no browser
				rdbtnBrowserNone.setSelected(true);
			} else if (browserCommand.equals(Settings.DEFAULT_BROWSER)) { // default
																			// browser
				rdbtnBrowserSystemDefault.setSelected(true);
			} else if (Settings.IE_BROWSER.equals(browserCommand)) { // IE
																		// browser
				rdbtnBrowserIE.setSelected(true);
			} else { // other browser
				rdbtnBrowserOther.setSelected(true);
				textField_webBrowserOther.setEnabled(true);
				textField_webBrowserOther.setText(new Browser(settings.browser()).getCommandString());
				button_webBrowserOther.setEnabled(true);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent event) {
						settings.setDocRoot(textField_webDirectory.getText());
						String browserCommand;
						if (rdbtnBrowserIE.isSelected()) {
							browserCommand = Settings.IE_BROWSER;
						} else if (rdbtnBrowserSystemDefault.isSelected()) {
							browserCommand = Settings.DEFAULT_BROWSER;
						} else if (rdbtnBrowserNone.isSelected()) {
							browserCommand = null;
						} else {
							JsonArray jBrowser = new JsonArray();
							jBrowser.addValue(new JsonString(textField_webBrowserOther.getText()));
							browserCommand = jBrowser.toString();
						}
						settings.setBrowser(browserCommand);
						try {
							settings.save();
						} catch (IOException e) {
							JOptionPane.showMessageDialog(SettingsDialog.this,
									"Could not save the settings.\nError: " + e);
						}
						SettingsDialog.this.setVisible(false);
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						SettingsDialog.this.setVisible(false);
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

}
