package porsud.µwebserver;

/**
 * @author Fabien Mairesse
 */
class StopFlag {

	private boolean stop = false;

	public synchronized void set() {
		this.stop = true;
		this.notifyAll();
	}

	public synchronized void waitForStop() {
		while (true) {
			if (this.stop)
				break;
			try {
				this.wait();
			} catch (InterruptedException e) {
			}
		}
	}
}
