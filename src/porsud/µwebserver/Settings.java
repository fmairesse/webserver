package porsud.µwebserver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Fabien Mairesse
 */
class Settings {
//	private static final int DEFAULT_PORT = 8080;
//	private static final String DEFAULT_ROOT_DIR = "www";
//	private static final boolean DEFAULT_SHOWEXITBUTTON = false;
//	private static final boolean DEFAULT_SHOWTRAY = true;
//	private static final int DEFAULT_HEARTBEATTIMEOUT = 0;
//	private static final int DEFAULT_SERVERCHECKTIMEOUT = 0;
	public static final String IE_BROWSER = "[\"C:\\\\Program Files\\\\Internet Explorer\\\\iexplore.exe\", \"-k\"]";
	public static final String DEFAULT_BROWSER = "[]";
	private static final String ANY_AVAILABLE_PORT = "*";
//	private static final String DEFAULT_BROWSER = IE_BROWSER;

	private static final String KEY_PORT = "port";
	private static final String KEY_DOCROOT = "docRoot";
	private static final String KEY_BROWSER = "browser";
	private static final String KEY_SHOWEXITBUTTON = "showExitButton";
	private static final String KEY_SHOWTRAY = "showTray";
	private static final String KEY_HEARTBEATTIMEOUT = "heartbeatTimeout";
	private static final String KEY_SERVERCHECKTIMEOUT = "serverCheckTimeout";
	private static final String KEY_STARTURL = "startUrl";

	private static final String FILENAME = "webserver.properties";

	private Properties properties = new Properties();
	private List<Runnable> savedCallbacks = new ArrayList<Runnable>();

	/**
	 * @param savedCallback Called when the properties are saved
	 */
	@SuppressWarnings("resource")
	public Settings() {

		InputStream in;
		// Try to load the settings from the current directory
		try {
			in = new FileInputStream(FILENAME);
		} catch (FileNotFoundException e) {
			// Load the settings bundled with this program
			in = Settings.class.getResourceAsStream(FILENAME);
			if (in == null) {
				throw new RuntimeException("Could not find '" + FILENAME + "' in the bundle");
			}
		}

		try {
			properties.load(in);
		} catch (IOException e) {
			throw new RuntimeException("Could not load the settings", e);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
			}
		}
	}

	public void save() throws IOException {
		OutputStream os = new FileOutputStream(FILENAME);
		try {
			properties.store(os, null);
			for (Runnable callback: this.savedCallbacks) {
				callback.run();
			}
		} finally {
			os.close();
		}
	}

	public void addSavedCallback(Runnable callback) {
		this.savedCallbacks.add(callback);
	}
	
	public void removeSavedCallback(Runnable callback) {
		this.savedCallbacks.remove(callback);
	}

	/**
	 * Returns an Integer or null if the port can be any available port.
	 * @throw NumberFormatException If the port could not be parsed as an integer.
	 */
	public Integer port() {
		String portAsString = this.getString(KEY_PORT, true);
		Integer port;
		if (ANY_AVAILABLE_PORT.equals(portAsString)) {
			port = null;
		} else {
			port = Integer.parseInt(portAsString);
		}
		return port;
	}

	public String docRoot() {
		return getString(KEY_DOCROOT, true);
	}
	public void setDocRoot(String docRoot) {
		this.setString(KEY_DOCROOT, docRoot);
	}
	
	public String startUrl() {
		String url = getString(KEY_STARTURL, false);
		if (url == null) {
			url = "";
		}
		return url;
	}
	
	public boolean showExitButton() {
		return this.getBoolean(KEY_SHOWEXITBUTTON);
	}

	public String browser() {
		return getString(KEY_BROWSER, false);
	}
	public void setBrowser(String browser) {
		this.setString(KEY_BROWSER, browser);
	}

	/**
	 * @throw NumberFormatException If the timeout could not be parsed as an integer.
	 */
	public int heartbeatTimeout() {
		return getInt(KEY_HEARTBEATTIMEOUT);
	}

	/**
	 * @throw NumberFormatException If the timeout could not be parsed as an integer.
	 */
	public int serverCheckTimeout() {
		return getInt(KEY_SERVERCHECKTIMEOUT);
	}

	public boolean showTray() {
		return getBoolean(KEY_SHOWTRAY);
	}

	private String getString(String key, boolean throwIfNotFound) {
		String value = this.properties.getProperty(key);
		if (value == null) {
			if (throwIfNotFound)
				throw new RuntimeException(String.format("Couldn't not find the property '%s' in the settings", key));
		} else {
			value = value.trim();
		}
		return value;
	}

	private void setString(String key, String value) {
		if (value == null) {
			this.properties.remove(key);
		} else {
			this.properties.setProperty(key, value);
		}
	}
	
	private boolean getBoolean(String key) {
		String valueStr = this.properties.getProperty(key);
		boolean value = valueStr.toLowerCase().equals("true");
		return value;
	}

	private int getInt(String key) {
		String valueStr = this.properties.getProperty(key);
		int value = Integer.parseInt(valueStr);
		return value;
	}
}