package porsud.µwebserver.resources;

import java.awt.Image;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

/**
 * Utility class to get the images of the application.
 * @author Fabien Mairesse
 */
public class Resources {
	private static Map<String, Image> imageCache = new HashMap<String, Image>();

	public static Image icon() {
		return getImage("icon.png");
	}

	public static Image connectedIcon() {
		return getImage("connected.png");
	}

	public static Image stopped() {
		return getImage("stopped.png");
	}

	public static InputStream getResource(String resourceName) {
		return Resources.class.getResourceAsStream(resourceName);
	}

	private static Image getImage(String imageName) {
		Image image = imageCache.get(imageName);
		if (image == null) {
			image = new ImageIcon(Resources.class.getResource(imageName)).getImage();
			imageCache.put(imageName, image);
		}
		return image;
	}
}
