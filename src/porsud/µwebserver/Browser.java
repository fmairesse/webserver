package porsud.µwebserver;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import porsud.json.JsonArray;
import porsud.json.JsonException;
import porsud.json.JsonParser;
import porsud.json.JsonString;
import porsud.json.JsonValue;


/**
 * Utility to handle the web browser.
 * @author Fabien Mairesse
 */
class Browser {

	private String[] browserCommand;

	public Browser(String browserCommand) {
		this.setCommand(browserCommand);
	}

	/**
	 * Constructs a Browser based on the browser command provided by the given Settings.
	 * The Browser will watch the given Settings.
	 */
	public Browser(final Settings settings) {
		this.setCommand(settings.browser());
		settings.addSavedCallback(new Runnable() {
			@Override
			public void run() {
				Browser.this.setCommand(settings.browser());
			}
		});
	}

	public void setCommand(String browserCommand) {
		if (browserCommand == null) {
			this.browserCommand = null;
		} else {
			// Parse browserCommand as a JSON array
			JsonArray jBrowserCommand;
			try {
				jBrowserCommand = JsonParser.parseArray(browserCommand);
			} catch (JsonException e) {
				jBrowserCommand = null;
			}
			if (jBrowserCommand == null) {
				// Leave command empty to launch default browser
				this.browserCommand = new String[0];
			} else {
				// Copy the content of the array
				int len = jBrowserCommand.length();
				if (len > 0) {
					this.browserCommand = new String[len+1]; 
					for (int i = 0; i < len; ++i) {
						JsonValue jvalue = jBrowserCommand.get(i);
						JsonString jvalueAsString = jvalue.isString();
						String value;
						if (jvalueAsString == null) {
							value = jvalue.toString();
						} else {
							value = jvalueAsString.value;
						}
						this.browserCommand[i] = value;
					}
				} else {
					this.browserCommand = new String[0];
				}
			}
		}
	}

	public void open(String url) throws IOException {
		if (! this.canOpen())
			return;

		if (this.browserCommand.length == 0) {
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (URISyntaxException e) {
			}
		} else {
			this.browserCommand[this.browserCommand.length-1] = url;
			Runtime.getRuntime().exec(this.browserCommand);
		}
	}

	public boolean canOpen() {
		return this.browserCommand != null;
	}

	public String getCommandString() {
		if (this.browserCommand == null)
			return null;
		StringBuilder b = new StringBuilder();
		for (int i = 0; i <= this.browserCommand.length-2; ++i) {
			if (i > 0) {
				b.append(' ');
			}
			b.append(this.browserCommand[i]);
		}
		return b.toString();
	}
}
