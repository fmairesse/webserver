package porsud.µwebserver;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.nio.protocol.BasicAsyncRequestConsumer;
import org.apache.http.nio.protocol.BasicAsyncResponseProducer;
import org.apache.http.nio.protocol.HttpAsyncExchange;
import org.apache.http.nio.protocol.HttpAsyncRequestConsumer;
import org.apache.http.nio.protocol.HttpAsyncRequestHandler;
import org.apache.http.protocol.HttpContext;

/**
 * @author Fabien Mairesse
 */
abstract class BasicHttpAsyncRequestHandler implements HttpAsyncRequestHandler<HttpRequest> {

	@Override
	public final HttpAsyncRequestConsumer<HttpRequest> processRequest(HttpRequest request, HttpContext context) throws HttpException, IOException {
		return new BasicAsyncRequestConsumer();
	}

	@Override
	public final void handle(HttpRequest request, HttpAsyncExchange httpExchange, HttpContext context) throws HttpException, IOException {
		HttpResponse response = httpExchange.getResponse();
		handle(request, response);
		httpExchange.submitResponse(new BasicAsyncResponseProducer(response));
	}

	protected abstract void handle(HttpRequest request, HttpResponse response) throws HttpException, IOException;
}