package porsud.µwebserver;

import java.util.Date;
import java.util.Map;

/**
 * A Runnable that checks regularly signs of life of http clients
 * and shutdown a given IOReactor if nobody seems alive.
 * @author Fabien Mairesse
 */
public class HeartbeatChecker implements Runnable {

	public int timeout;
	private Map<String, Date> heartbeats;
	private StopFlag stopFlag;
	private boolean stop = false;

	/**
	 * @param heartbeats Maps a client identifier to its last heartbeat time
	 * @param ioReactor The IOReactor to shutdown when everybody is dead
	 * @param timeout The time between two heartbeats beyond which a client is considered as dead 
	 */
	public HeartbeatChecker(Map<String, Date> heartbeats, StopFlag stopFlag, int timeout) {
		this.heartbeats = heartbeats;
		this.stopFlag = stopFlag;
		this.timeout = timeout;
	}

	@Override
	public void run() {
		if (this.timeout <= 0)
			return;

		while (true) {
			synchronized (this) {
				if (this.stop) {
					break;
				}
				try {
					this.wait(timeout);
				} catch (InterruptedException e) {
				}
				if (this.stop) {
					break;
				}

				// Stop only if no recent heartbeats from all clients
				boolean stop = true;
				long now = new Date().getTime();
				for (Map.Entry<String, Date> entry : this.heartbeats.entrySet()) {
					if (now - entry.getValue().getTime() < timeout) {
						stop = false;
						break;
					}
				}
				if (stop) {
					this.stopFlag.set();
					break;
				}
			}
		}
	}

	public synchronized void stop() {
		this.stop = true;
	}

}