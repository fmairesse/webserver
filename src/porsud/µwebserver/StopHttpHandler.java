package porsud.µwebserver;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;

/**
 * When called, this handler terminates a given IOReactor.
 * @author Fabien Mairesse
 */
class StopHttpHandler extends BasicHttpAsyncRequestHandler {

	private StopFlag stopFlag;

	public StopHttpHandler(StopFlag stopFlag) {
		if (stopFlag == null) {
			throw new IllegalArgumentException();
		}
		this.stopFlag = stopFlag;
	}

	@Override
	public void handle(HttpRequest request, HttpResponse response) throws HttpException, IOException {
		this.stopFlag.set();
	}
}