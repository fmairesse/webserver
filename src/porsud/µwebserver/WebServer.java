package porsud.µwebserver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.config.ConnectionConfig;
import org.apache.http.impl.nio.DefaultHttpServerIODispatch;
import org.apache.http.impl.nio.DefaultNHttpServerConnection;
import org.apache.http.impl.nio.DefaultNHttpServerConnectionFactory;
import org.apache.http.impl.nio.reactor.DefaultListeningIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.nio.NHttpConnectionFactory;
import org.apache.http.nio.NHttpServerConnection;
import org.apache.http.nio.protocol.HttpAsyncService;
import org.apache.http.nio.protocol.UriHttpAsyncRequestHandlerMapper;
import org.apache.http.nio.reactor.IOEventDispatch;
import org.apache.http.nio.reactor.IOReactor;
import org.apache.http.nio.reactor.IOReactorException;
import org.apache.http.nio.reactor.IOReactorStatus;
import org.apache.http.nio.reactor.ListeningIOReactor;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpProcessorBuilder;
import org.apache.http.protocol.ResponseConnControl;
import org.apache.http.protocol.ResponseContent;
import org.apache.http.protocol.ResponseDate;
import org.apache.http.protocol.ResponseServer;


/**
 * @author Fabien Mairesse
 */
public class WebServer {

	/**
	 * Watches the connection state of the web server.
	 */
	public interface ConnectionListener {
		void connectionOpen();
		void connectionClosed();
	}
	
	public interface Configuration {
		int port();
		String docRoot();
		boolean showExitButton();
		/**
		 * The delay in seconds between two heartbeats beyond which a client is considered as dead.
		 * If 0, the server wont do such a check.
		 */
		int heartbeatTimeout();
		/**
		 * Returns the maximum time to wait to check if the server can be reached
		 * and set the running status accordingly.
		 * If 0, wait indefinetely.
		 */
		int serverCheckTimeout();
	}

	public enum RunningStatus {
		STARTING, SERVING, STOPPED
	}

	private IOReactor ioReactor;
	private Configuration configuration;
	private RunningStatus runningStatus = RunningStatus.STOPPED;
	private Collection<ConnectionListener> connectionListeners = new ArrayList<WebServer.ConnectionListener>();
	private HeartbeatChecker heartbeatChecker;
	private StopFlag stopFlag;

	public WebServer(Configuration configuration, StopFlag stopFlag) {
		if (configuration == null) {
			throw new IllegalArgumentException();
		}
		this.configuration = configuration;
		this.stopFlag = stopFlag;
	}

	public void addConnectionListener(ConnectionListener listener) {
		this.connectionListeners.add(listener);
	}

	public String getRootURL(String baseURL) {
		return WebServer.getRootURL(this.configuration.port(), baseURL);
	}

	public static String getRootURL(int port, String baseURL) {
		return "http://localhost:" + port + "/" + baseURL;
	}

	/**
	 * @throws FileNotFoundException If the root web directory is not a valid directory
	 * @throws IOReactorException If the IOReactor could not be created
	 */
	public void start() throws IOException {
		// Check doc root exists
		File docRoot = new File(new File(this.configuration.docRoot()).getAbsolutePath());
		if (!docRoot.isDirectory()) {
			throw new FileNotFoundException("Document root directory does not exists: " + this.configuration.docRoot());
		}

		setRunningStatus(RunningStatus.STARTING);

		// Set I/O reactor defaults
		IOReactorConfig config = IOReactorConfig.custom().setIoThreadCount(1).setSoTimeout(3000).setConnectTimeout(3000).build();
		// Create server-side I/O reactor
		ListeningIOReactor listeningIOReactor;
		try {
			listeningIOReactor = new DefaultListeningIOReactor(config);
		} finally {
			setRunningStatus(RunningStatus.STOPPED);
		}
		this.ioReactor = listeningIOReactor;

		// Start heartbeat checker to stop when nobody has been connected for a delay
		Map<String, Date> heartbeats = new HashMap<String, Date>();
		this.heartbeatChecker = new HeartbeatChecker(heartbeats, stopFlag, this.configuration.heartbeatTimeout());
		new Thread(this.heartbeatChecker, "HeartbeatChecker").start();

		// Create HTTP protocol processing chain
		HttpProcessor httpproc = HttpProcessorBuilder.create().
			add(new ResponseDate()).
			add(new ResponseServer("µWebServer")).
			add(new ResponseContent()).
			add(new ResponseConnControl()).
			build();

		// Create request handler registry
		UriHttpAsyncRequestHandlerMapper registry = new UriHttpAsyncRequestHandlerMapper();
		ImagesHttpHandler imagesHandler = new ImagesHttpHandler(docRoot);
		registry.register("*.jpg", imagesHandler);
		registry.register("*.jpeg", imagesHandler);
		registry.register("*.png", imagesHandler);
		registry.register(Protocol.Utils.getPath(Protocol.Heartbeat.class), new HeartBeatHttpHandler(heartbeats, this.configuration.heartbeatTimeout()));
		registry.register(Protocol.Utils.getPath(Protocol.Stop.class), new StopHttpHandler(this.stopFlag));
		registry.register(Protocol.Utils.getPath(Protocol.Configuration.class), new ConfigurationHttpHandler(this.configuration.showExitButton()));
		registry.register("*", new HttpFileHandler(docRoot));

		// Create server-side HTTP protocol handler
		HttpAsyncService protocolHandler = new HttpAsyncService(httpproc, registry) {
			@Override
			public void connected(final NHttpServerConnection conn) {
				for (ConnectionListener listener : WebServer.this.connectionListeners) {
					listener.connectionOpen();
				}
				super.connected(conn);
			}
			@Override
			public void closed(final NHttpServerConnection conn) {
				super.closed(conn);
				for (ConnectionListener listener : WebServer.this.connectionListeners) {
					listener.connectionClosed();
				}
			}
		};

		// Create HTTP connection factory
		NHttpConnectionFactory<DefaultNHttpServerConnection> connFactory = new DefaultNHttpServerConnectionFactory(ConnectionConfig.DEFAULT);
		// Create server-side I/O event dispatch
		final IOEventDispatch ioEventDispatch = new DefaultHttpServerIODispatch(protocolHandler, connFactory);

		// Listen of the given port
		listeningIOReactor.listen(new InetSocketAddress(this.configuration.port()));

		// Launch monitoring of the server
		new Thread(new ServerCheck(), "ServerCheck").start();

		// Ready to go!
		new Thread("ServerIO") {
			public void run() {
				try {
					WebServer.this.ioReactor.execute(ioEventDispatch);
					
					// Notify heartbeatChecker to stop it as soon as possible
					synchronized (WebServer.this.heartbeatChecker) {
						WebServer.this.heartbeatChecker.notifyAll();
					}

					// Update server status
					WebServer.this.setRunningStatus(RunningStatus.STOPPED);
					
					System.out.println("Shutdown");
				} catch (IOException e) {
					System.err.println("I/O error: " + e.getMessage());
				}
			}
		}.start();
	}

	public void stop() {
		if (this.ioReactor != null && this.ioReactor.getStatus() != IOReactorStatus.SHUT_DOWN) {
			this.heartbeatChecker.stop();
			try {
				this.ioReactor.shutdown();
			} catch (IOException e) {
			}
		}
	}

	public void restart() throws IOException {
		this.stop();
		synchronized(this) {
			while (this.runningStatus != RunningStatus.STOPPED) {
				try {
					wait(100);
				} catch (InterruptedException e) {
				}
			}
		}
		this.start();
	}

	public synchronized RunningStatus getRunningStatus() {
		return this.runningStatus;
	}

	public void waitForRunningStatus(RunningStatus status) {
		while (true) {
			synchronized (this) {
				if (this.runningStatus == status) {
					break;
				}
				try {
					this.wait(100);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	private void setRunningStatus(RunningStatus status) {
		synchronized (this) {
			this.runningStatus = status;
			this.notifyAll();
		}
	}

	/**
	 * Tries to establish a connection to the server to check it is running
	 */
	private class ServerCheck implements Runnable {

		private static final int POLL_PERIOD = 100;

		@Override
		public void run() {
			long startTime = new Date().getTime();
			Object monitor = new Object();
			HttpURLConnection connection;

			while (true) {
				// Stop if server is not running anymore
				if (WebServer.this.ioReactor.getStatus() == IOReactorStatus.SHUT_DOWN) {
					WebServer.this.setRunningStatus(RunningStatus.STOPPED);
					break;
				}

				// Stop if we have checked enough time
				long now = new Date().getTime();
				int timeout = WebServer.this.configuration.serverCheckTimeout();
				if (timeout > 0 && now > startTime + timeout) {
					try {
						ioReactor.shutdown();
					} catch (IOException e) {
					}
					continue;
				}
				
				// Try to connect to the server
				try {
					connection = (HttpURLConnection) new URL(WebServer.this.getRootURL("")).openConnection();
					connection.setReadTimeout(timeout);
					connection.setRequestMethod("HEAD");
					connection.connect();
					WebServer.this.setRunningStatus(RunningStatus.SERVING);
					break;
				} catch (ProtocolException e) {
					throw new RuntimeException(e);
				} catch (MalformedURLException e) {
					throw new RuntimeException(e);
				} catch (IOException e) {
					// do nothing, we'll try again
				}

				// Wait before retrying
				try {
					synchronized(monitor) {
						monitor.wait(POLL_PERIOD);
					}
				} catch (InterruptedException e1) {
					break;
				}
			}
		}
	}
}
