package porsud.json;

import java.io.IOException;

public final class JsonNull extends JsonValue {

	static final String NULL = "null";

	public static final JsonNull NULL_VALUE = new JsonNull();

	private JsonNull() {
	}

	public static boolean equals(JsonNull a, JsonNull b) {
		return a == b;
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof JsonNull))
			return false;
		return equals((JsonNull) o, this);
	}

	public void print(Writer writer) throws IOException {
		writer.write(NULL);
	}

	public JsonArray isArray() {
		return null;
	}

	public JsonBoolean isBoolean() {
		return null;
	}

	public JsonNull isNull() {
		return this;
	}

	public JsonNumber isNumber() {
		return null;
	}

	public JsonObject isObject() {
		return null;
	}

	public JsonString isString() {
		return null;
	}

}
