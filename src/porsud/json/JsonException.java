package porsud.json;

public final class JsonException extends Exception {

	private static final long serialVersionUID = 4339859520046228210L;

	public JsonException() {
		super();
	}

	public JsonException(String message) {
		super(message);
	}

	public JsonException(String message, Throwable cause) {
		super(message, cause);
	}
}
