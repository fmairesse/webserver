package porsud.json;

import java.io.IOException;


public final class JsonBoolean extends JsonValue {

	static final String TRUE = "true";
	static final String FALSE = "false";

	public static final JsonBoolean TRUE_VALUE = new JsonBoolean(true);
	public static final JsonBoolean FALSE_VALUE = new JsonBoolean(false);

	private final boolean value;

	public JsonBoolean(boolean value) {
		this.value = value;
	}
	
	public boolean isTrue() {
		return value;
	}

	public static boolean equals(JsonBoolean a, JsonBoolean b) {
		return a.value == b.value;
	}
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof JsonBoolean))
			return false;
		return equals((JsonBoolean) o, this);
	}

	public void print(Writer writer) throws IOException {
		writer.write(value ? TRUE : FALSE);
	}

	public JsonArray isArray() {
		return null;
	}

	public JsonBoolean isBoolean() {
		return this;
	}

	public JsonNull isNull() {
		return null;
	}

	public JsonNumber isNumber() {
		return null;
	}

	public JsonObject isObject() {
		return null;
	}

	public JsonString isString() {
		return null;
	}

}
