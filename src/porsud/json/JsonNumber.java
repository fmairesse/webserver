package porsud.json;

import java.io.IOException;

public final class JsonNumber extends JsonValue {

	public final Number number;

	public JsonNumber(Number number) {
		this.number = number;
	}

	public static boolean equals(JsonNumber a, JsonNumber b) {
		return a.number.equals(b.number);
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof JsonNumber))
			return false;
		return equals((JsonNumber) o, this);
	}

	public void print(Writer writer) throws IOException {
		writer.write(number.toString());
	}

	public JsonArray isArray() {
		return null;
	}

	public JsonBoolean isBoolean() {
		return null;
	}

	public JsonNull isNull() {
		return null;
	}

	public JsonNumber isNumber() {
		return this;
	}

	public JsonObject isObject() {
		return null;
	}

	public JsonString isString() {
		return null;
	}

}
