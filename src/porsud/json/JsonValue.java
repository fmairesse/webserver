package porsud.json;

import java.io.IOException;

public abstract class JsonValue {

	public final String toString() {
		StringBuilderWriter writer = new StringBuilderWriter();
		try {
			print(writer);
		} catch (IOException e) {
		}
		return writer.builder.toString();
	}

	public abstract void print(Writer writer) throws IOException;

	public abstract JsonNull isNull();

	public abstract JsonNumber isNumber();

	public abstract JsonString isString();

	public abstract JsonArray isArray();

	public abstract JsonObject isObject();

	public abstract JsonBoolean isBoolean();

	public static boolean equals(JsonValue a, JsonValue b) {
		JsonNull n = a.isNull();
		if (n != null)
			return JsonNull.equals(n, b.isNull());
		JsonNumber nu = a.isNumber();
		if (nu != null)
			return JsonNumber.equals(nu, b.isNumber());
		JsonString s = a.isString();
		if (s != null)
			return JsonString.equals(s, b.isString());
		JsonArray ar = a.isArray();
		if (ar != null)
			return JsonArray.equals(ar, b.isArray());
		JsonObject oo = a.isObject();
		if (oo != null)
			return JsonObject.equals(oo, b.isObject());
		JsonBoolean bo = a.isBoolean();
		if (b != null)
			return JsonBoolean.equals(bo, b.isBoolean());
		return false;
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof JsonValue))
			return false;
		return equals((JsonValue) o, this);
	}

	public static interface Writer {
		void write(char c) throws IOException;

		void write(String s) throws IOException;
	}

	private static final class StringBuilderWriter implements Writer {
		private final StringBuilder builder;

		public StringBuilderWriter() {
			this(new StringBuilder());
		}

		public StringBuilderWriter(StringBuilder builder) {
			this.builder = builder;
		}

		public void write(char c) {
			builder.append(c);
		}

		public void write(String s) {
			builder.append(s);
		}
	}
}
