package porsud.json;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public final class JsonObject extends JsonValue {

	private final Map<String, JsonValue> members = new HashMap<String, JsonValue>();

	public JsonObject() {
	}

//	public Iterable<Pair<String, JsonValue>> pairs() {
//		return new PairFromMapEntryIterable<String, JsonValue>(members.entrySet());
//	}

	public static boolean equals(JsonObject a, JsonObject b) {
		if (a.members.size() != b.members.size())
			return false;
		for (Map.Entry<String, JsonValue> e : a.members.entrySet()) {
			JsonValue v = b.members.get(e.getKey());
			if (v == null)
				return false;
			if (!JsonValue.equals(e.getValue(), v))
				return false;
		}
		return true;
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof JsonObject))
			return false;
		return equals((JsonObject) o, this);
	}

	public void put(String key, JsonValue value) {
		members.put(key, value);
	}

	public void remove(String key) {
		members.remove(key);
	}

	public JsonValue get(String key) {
		return members.get(key);
	}

	public void print(Writer writer) throws IOException {
		writer.write('{');
		boolean first = true;
		for (Map.Entry<String, JsonValue> member : members.entrySet()) {
			if (first)
				first = false;
			else
				writer.write(',');
			new JsonString(member.getKey()).print(writer);
			writer.write(':');
			member.getValue().print(writer);
		}
		writer.write('}');
	}

	public JsonArray isArray() {
		return null;
	}

	public JsonBoolean isBoolean() {
		return null;
	}

	public JsonNull isNull() {
		return null;
	}

	public JsonNumber isNumber() {
		return null;
	}

	public JsonObject isObject() {
		return this;
	}

	public JsonString isString() {
		return null;
	}

}
