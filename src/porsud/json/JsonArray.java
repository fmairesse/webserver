package porsud.json;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class JsonArray extends JsonValue implements Iterable<JsonValue> {

	public static final char BEGIN = '[';
	public static final char END = ']';
	public static final char SEPARATOR = ',';
	
	private List<JsonValue> values = new LinkedList<JsonValue>();

	public JsonArray() {
	}

	public static boolean equals(JsonArray a, JsonArray b) {
		if (a.values.size() != b.values.size())
			return false;
		Iterator<JsonValue> i = a.values.iterator();
		Iterator<JsonValue> j = b.values.iterator();
		while (true) {
			if (!i.hasNext())
				break;
			if (!j.hasNext())
				break;
			JsonValue aa = i.next();
			JsonValue bb = j.next();
			if (!JsonValue.equals(aa, bb))
				return false;
		}
		return true;
	}
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof JsonArray))
			return false;
		return equals((JsonArray) o, this);
	}

	public void addValue(JsonValue value) {
		values.add(value);
	}

	public Iterator<JsonValue> iterator() {
		return values.iterator();
	}
	
	public JsonValue get(int index) {
		return values.get(index);
	}

	public int length() {
		return values.size();
	}

	public void print(Writer writer) throws IOException {
		writer.write(BEGIN);
		boolean first = true;
		for (JsonValue value : values) {
			if (first)
				first = false;
			else
				writer.write(SEPARATOR);
			value.print(writer);
		}
		writer.write(END);
	}

	public JsonArray isArray() {
		return this;
	}

	public JsonBoolean isBoolean() {
		return null;
	}

	public JsonNull isNull() {
		return null;
	}

	public JsonNumber isNumber() {
		return null;
	}

	public JsonObject isObject() {
		return null;
	}

	public JsonString isString() {
		return null;
	}

}
