package porsud.json;

import java.io.IOException;

public final class JsonString extends JsonValue {

	public final String value;

	public JsonString(String value) {
		if (value == null)
			throw new NullPointerException();
		this.value = value;
	}

	public JsonString(char c) {
		this.value = Character.toString(c);
	}

	public static boolean equals(JsonString a, JsonString b) {
		return a.value.equals(b.value);
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof JsonString))
			return false;
		return equals((JsonString) o, this);
	}

//	public int hashCode() {
//		return value.hashCode();
//	}

	public void print(Writer writer) throws IOException {
//NON		if (value == null) {
//			writer.write("\"\"");
//			return;
//		}

		char b;
		char c = 0;
		int i;
		int len = value.length();
		String t;

		writer.write('"');
		for (i = 0; i < len; i += 1) {
			b = c;
			c = value.charAt(i);
			switch (c) {
			case '\\':
			case '"':
				writer.write('\\');
				writer.write(c);
				break;
			case '/':
				if (b == '<') {
					writer.write('\\');
				}
				writer.write(c);
				break;
			case '\b':
				writer.write("\\b");
				break;
			case '\t':
				writer.write("\\t");
				break;
			case '\n':
				writer.write("\\n");
				break;
			case '\f':
				writer.write("\\f");
				break;
			case '\r':
				writer.write("\\r");
				break;
			default:
				if (c < ' ' || (c >= '\u0080' && c < '\u00a0') || (c >= '\u2000' && c < '\u2100')) {
					t = "000" + Integer.toHexString(c);
					writer.write("\\u" + t.substring(t.length() - 4));
				} else {
					writer.write(c);
				}
			}
		}
		writer.write('"');
	}

	public JsonArray isArray() {
		return null;
	}

	public JsonBoolean isBoolean() {
		return null;
	}

	public JsonNull isNull() {
		return null;
	}

	public JsonNumber isNumber() {
		return null;
	}

	public JsonObject isObject() {
		return null;
	}

	public JsonString isString() {
		return this;
	}

}
